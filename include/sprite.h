#ifndef DEBUG_LINE_H
#define DEBUG_LINE_H


#include <raylib.h>

struct Sprite {
     Texture2D texture;
     Vector2 frameSize;
     int maxFrame;
     int framesWide;
     Vector2 origin;
     int frame;
     void draw(float x, float y, float ang, float scale, Color c);
     void next_frame();
 };

#endif