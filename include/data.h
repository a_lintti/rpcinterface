
#ifndef DATA_H
#define DATA_H

#include <map>

#include "rayman.h"


float spice_table[7][7] = {
    {1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0},
    {1.0, 2.0, 1.0, 0.5, 2.0, 0.5, 0.5},
    {1.0, 1.0, 1.0, 1.0, 0.5, 2.0, 0.5},
    {1.0, 2.0, 2.0, 0.5, 0.5, 1.0, 1.0},
    {1.0, 0.5, 0.5, 2.0, 1.0, 2.0, 0.5},
    {1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0},
    {2.0, 1.0, 2.0, 0.5, 1.0, 0.5, 2.0}
};

std::map<int, Spice> spice_map() {
    std::map<int, Spice> m;
    m[0] = Spice("Kurkuma", 0);
    m[1] = Spice("Suola", 1);
    m[2] = Spice("Pippuri", 2);
    m[3] = Spice("Sokeri", 3);
    m[4] = Spice("Basilika", 4);
    m[5] = Spice("Chili", 5);
    m[6] = Spice("Mauste", 6);
}

std::map<int, Gun> gun_map() {
    std::map<int, Gun> m;
    m[0] = Gun("Tussari", "2 stronk - rolli 6-20 - Lyhyen matkan tuli ase. Kolmen vierekkäisen ruudun AOE, joista vähintään yhden oltava kantamassa.", 0, 1, 5, 1.5);
    m[1] = Gun("Sniperkivääri", "Häyhäilyyn - rolli 9-20 - Pitkän matkan tuliase.", 1, 4, 11);
    m[2] = Gun("CQC+", "Jos lyö toista niin se sattuu. - ei vaadi rollia - Lyönti", 2, 0, 1, 1.5);
    m[3] = Gun("Omaewa", "Mou shindeiru - rolli 6-20 - Teleporttaa käyttäjän kantamassa olevan kohteen taakse (ignoraa suojat). Rolli vaaditaan ainoastaan damageen.", 3, 1, 6);
    m[4] = Gun("Buffi", "On a watchlist - rolli 9-20 - Kaikki kantamassa olevat unitit saavat +20 atk seuraavalle vuorolleen.", 4, 0, 4);
    m[5] = Gun("Hiili", " - rolli 6-20 - Parantaa kohdetta flat +50.", 5, 1, 8);
    m[6] = Gun("ATP", "Aineenvaihdunnan perusyksikkö - rolli määrää vaikutuksen - ", 6, 0, 5);
    m[7] = Gun("Antitankkiase", "Älä hommaa legendarylle demolitionia - rolli 6-20 - 2.0x damage tankkeihin; 0.5x damage muihin raymaneihin.", 7, 1, 8);
    m[8] = Gun("Trap ansa", " - ei vaadi rollia - Asettaa ansan, joka aktivoituu ja tekee damagea 7 ruudun AOE:lla.", 8, 1, 10);
    m[9] = Gun("Lifesteal", " - ei vaadi rollia - Healaa damagen verran.", 9, 0, 2);
    m[10] = Gun("Batturabensas", " - rolli 11-20 - Basilika heittää batturabensaan siemenen, mistä kasvaa 10 ruudun kokoinen bensas. Bensas tekee damagea 3x d20. Ei tehoa basilika tyyppisiin raymaneihin", 10, 0, 6);
    m[11] = Gun("Maitohappo", "Lukonmäki on aika jyrkkä - rolli 6-20 - Antaa kohteelle +3 LP:tä seuraavalle liikkumiselle ja +50 def kunnes kohde käyttää vuoronsa seuraavalla kierroksella.", 11, 0, 5);
    m[12] = Gun("Tabascopesu", " - rolli 6-20 - Chili heittää tabascoa kohteen päälle ja kohde syttyy palamaan. Kohde ottaa damagea vuoronsa alussa ja jokaisella liikutulla ruudulla 1x d20. Kohde sammuttaa itsensä jos se pysyy vuoron paikallaan.", 12, 1, 5);
    m[13] = Gun("Spoonful of salt", " - rolli 6-20 - Vie aktioita, damagerolli määrää lukumäärän: 10-14 -> 1 | 15-19 -> 2 | 20 -> kaikki.", 13, 1, 8);
    m[14] = Gun("Bo-Boppuri", " - rolli 11-20 - Summoni, ATK summonaajan TP 50 Def 50 LP 3, vuoro summonaajan jälkeen, räjähtää kun käyttää räjähdysaktion tai tuhottaessa.", 14, 1, 5);
    m[15] = Gun("Maustetee", "The spice must flow - Antaa pysyviä vaikutuksia; rolli 1: käyttäjä kuolee, rolli 2-19: käyttäjään kohdistetut rollit -2 ja käyttäjän def -10, rolli 20: def = 0 ja atk x4 ja omat rollit +5.", 15, 0, 0);
}


#endif