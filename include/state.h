
#ifndef STATE_H
#define STATE_H

#include <vector>
#include <map>

#include "rayman.h"


class State {
    public:
        std::map<int, Rayman> raymans;
        std::map<int, Team> teams;
        std::map<int, Gun> guns;
        std::map<int, Spice> spices;
        float spice_table[7][7] = {
            {1.0, 1.0, 1.0, 2.0, 1.0, 1.0, 1.0},
            {1.0, 2.0, 1.0, 0.5, 2.0, 0.5, 0.5},
            {1.0, 1.0, 1.0, 1.0, 0.5, 2.0, 0.5},
            {1.0, 2.0, 2.0, 0.5, 0.5, 1.0, 1.0},
            {1.0, 0.5, 0.5, 2.0, 1.0, 2.0, 0.5},
            {1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0},
            {2.0, 1.0, 2.0, 0.5, 1.0, 0.5, 2.0}
        };
        int right_side;
        State() {
            right_side = 0;
            guns[0] = Gun("Tussari", "2 stronk - rolli 6-20 - Lyhyen matkan tuli ase. Kolmen vierekkaisen ruudun AOE, joista vahintaan yhden oltava kantamassa.", 0, 1, 5, 1.5);
            guns[1] = Gun("Sniperkivaari", "Hayhailyyn - rolli 9-20 - Pitkan matkan tuliase.", 1, 4, 11);
            guns[2] = Gun("CQC+", "Jos lyo toista niin se sattuu. - ei vaadi rollia - Lyonti", 2, 0, 1, 1.5);
            guns[3] = Gun("Omaewa", "Mou shindeiru - rolli 6-20 - Teleporttaa kayttajan kantamassa olevan kohteen taakse (ignoraa suojat). Rolli vaaditaan ainoastaan damageen.", 3, 1, 6);
            guns[4] = Gun("Buffi", "On a watchlist - rolli 9-20 - Kaikki kantamassa olevat unitit saavat +20 atk seuraavalle vuorolleen.", 4, 0, 4);
            guns[5] = Gun("Hiili", " - rolli 6-20 - Parantaa kohdetta flat +50.", 5, 1, 8);
            guns[6] = Gun("ATP", "Aineenvaihdunnan perusyksikko - rolli maaraa vaikutuksen - ", 6, 0, 5);
            guns[7] = Gun("Antitankkiase", "ala hommaa legendarylle demolitionia - rolli 6-20 - 2.0x damage tankkeihin; 0.5x damage muihin raymaneihin.", 7, 1, 8);
            guns[8] = Gun("Trap ansa", " - ei vaadi rollia - Asettaa ansan, joka aktivoituu ja tekee damagea 7 ruudun AOE:lla.", 8, 1, 10);
            guns[9] = Gun("Lifesteal", " - ei vaadi rollia - Healaa damagen verran.", 9, 0, 2);
            guns[10] = Gun("Batturabensas", " - rolli 11-20 - Basilika heittaa batturabensaan siemenen, mista kasvaa 10 ruudun kokoinen bensas. Bensas tekee damagea 3x d20. Ei tehoa basilika tyyppisiin raymaneihin", 10, 0, 6);
            guns[11] = Gun("Maitohappo", "Lukonmaki on aika jyrkka - rolli 6-20 - Antaa kohteelle +3 LP:ta seuraavalle liikkumiselle ja +50 def kunnes kohde kayttaa vuoronsa seuraavalla kierroksella.", 11, 0, 5);
            guns[12] = Gun("Tabascopesu", " - rolli 6-20 - Chili heittaa tabascoa kohteen paalle ja kohde syttyy palamaan. Kohde ottaa damagea vuoronsa alussa ja jokaisella liikutulla ruudulla 1x d20. Kohde sammuttaa itsensa jos se pysyy vuoron paikallaan.", 12, 1, 5);
            guns[13] = Gun("Spoonful of salt", " - rolli 6-20 - Vie aktioita, damagerolli maaraa lukumaaran: 10-14 -> 1 | 15-19 -> 2 | 20 -> kaikki.", 13, 1, 8);
            guns[14] = Gun("Bo-Boppuri", " - rolli 11-20 - Summoni, ATK summonaajan TP 50 Def 50 LP 3, vuoro summonaajan jalkeen, rajahtaa kun kayttaa rajahdysaktion tai tuhottaessa.", 14, 1, 5);
            guns[15] = Gun("Maustetee", "The spice must flow - Antaa pysyvia vaikutuksia; rolli 1: kayttaja kuolee, rolli 2-19: kayttajaan kohdistetut rollit -2 ja kayttajan def -10, rolli 20: def = 0 ja atk x4 ja omat rollit +5.", 15, 0, 0);
            
            spices[0] = Spice("Kurkuma", 0);
            spices[1] = Spice("Suola", 1);
            spices[2] = Spice("Pippuri", 2);
            spices[3] = Spice("Sokeri", 3);
            spices[4] = Spice("Basilika", 4);
            spices[5] = Spice("Chili", 5);
            spices[6] = Spice("Mauste", 6);
        }
    private:

};



#endif