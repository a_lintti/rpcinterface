
#ifndef RAYMAN_H
#define RAYMAN_H

#include <string>
#include <vector>

struct Spice {
    std::string name;
    int id;
    Spice() {
        name = "Kurkuma";
        id = 0;
    }
    Spice(std::string name_, int id_) {
        name = name_;
        id = id_;
    }
};

struct Ability {
    std::string name;
    std::string description;
    int scale;
};

struct Gun {
    std::string name;
    std::string description;
    int id;
    int min_range;
    int max_range;
    float multiplier;
    Gun() {
        name = "default lyönti";
        description = "Huono mutta hyödyllinen.";
        id = -1;
        min_range = 0;
        max_range = 1;
        multiplier = 0.5;
    }
    Gun(std::string n, std::string d, int id, int mir, int mar, float m):
        name(n), description(d), id(id), min_range(mir), max_range(mar), multiplier(m) {}
    Gun(std::string n, std::string d, int id, int mir, int mar):
        name(n), description(d), id(id), min_range(mir), max_range(mar) {
        multiplier = 1.0;
    }
    std::vector<std::string> to_string();
};

struct Rayman {
    std::string name;
    int id;
    int tp;
    int def;
    int atk;
    int lp;
    // type = {unit 0 / mount 1 / vehicle 2}
    int type;
    std::vector<Spice> spices;
    int actions;
    int range_adv;
    bool turn_used;
    std::vector<Ability> ablities;
    std::vector<Gun> guns;

    Rayman() {
        name = "";
        id = 0;
        tp = 60;
        def = 60;
        atk = 60;
        lp = 5;
        type = 0;
        std::vector<Spice> s;
        s.push_back(Spice());
        spices = s;
        actions = 2;
        range_adv = 0;
        turn_used = false;
    }

    Rayman(std::string name_,
                   int id_,
                   int tp_,
                   int def_,
                   int atk_,
                   int lp_,
                   int type_,
                   std::vector<Spice> spices_,
                   int actions_, 
                   int range_adv_) {
        name = name_;
        id = id_;
        tp = tp_;
        def = def_;
        atk = atk_;
        lp = lp_;
        type = type_;
        spices = spices_;
        actions = actions_;
        range_adv = range_adv_;
        turn_used = false;
    }

    std::vector<std::string> to_string();
};

struct Team {
    std::string name;
    int id;
    std::vector<Rayman*> raymans;

    Team() {
        name = "";
        id = 0;
    }

    Team(std::string name_, int id_, std::vector<Rayman*> raymans_) {
        name = name_;
        id = id_;
        raymans = raymans_;
    }
};

float spice_advantage(Rayman def, Rayman att);

int damage(float multiplier, int def, int atk, int roll);
int damage(Rayman def, Rayman att, Gun gun, int roll);

#endif

