#ifndef DEBUG_LINE_H
#define DEBUG_LINE_H

#include <vector>
#include <string>
#include <map>

#include <raylib.h>

#include "state.h"


class DebugLine {
    public:
        DebugLine();
        void update(State &state);
        void set_position(int x, int y);
        void draw(Font font);
        void move_cursor(bool direction);
        void add_letter(char letter);
        void remove_letter();
        void execute_line(State &state);
        std::vector<std::string> list_commands();
    private:
        int x;
        int y;
        int width;
        int height;
        int font_size;
        int line_height;
        int cursor_pos;
        std::string current_line;
        std::vector<std::string> history;
        std::vector<std::string> content;
        std::map<std::string, int> command_map;
        std::map<int, char> keycode_to_char_map;
        void update_content();
        void add_content(std::string c);
};



#endif