#ifndef INTERFACE_H
#define INTERFACE_H

#include <string>
#include <vector>
#include <map>
#include <raylib.h>
#include <rayman.h>

#define MOUSE_LEFT      0
#define FONT_SIZE_S     14
#define FONT_SIZE_M     20
#define FONT_SIZE_L     30

enum class ProgramStateType {
	Close, 
	AllTeams, 
	Team, 
	Rayman,
	DamageCalc,
	Test
};

struct ProgramState {
	ProgramStateType type;
	Team team;
	Rayman rayman;

    ProgramState() {
        type = ProgramStateType::AllTeams;
    }
    
    ProgramState(ProgramStateType type_) {
        type = type_;
    }
};

enum class ButtonState {
    Neutral,
    Inactive,
    Down,
    Hower
};

struct Button {
    // properties
    int width;
    int height;
    int x; 
    int y;
    std::string text;
    Color color_neutral;
    Color color_inactive;
    Color color_down;
    Color color_hower;
    Color color_font;
    int action;
    ButtonState state;
    // functions
    void draw();
    void set_state();
    bool pressed();
};

struct Text {
    int x;
    int y;
    Color color;
    int font_size;
    std::string text;
    
    void draw();
    void set_text(std::string);
};

struct Interface {
    // objects
    std::vector<Button*> buttons;
    std::vector<Text*> texts;

    void draw();
    int input();
};

ProgramState loop(ProgramState, std::vector<Team>);

#endif
