

spice = {
    "kurkuma": 0,
    "suola" : 1,
    "pippuri" : 2,
    "sokeri" : 3,
    "basilika": 4,
    "chili" : 5
}

multiplier = [
                [0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                [0.0, 1.5, 0.0, 0.5, 1.5, 0.5],
                [0.0, 0.0, 0.0, 0.0, 0.5, 1.5],
                [0.0, 1.5, 1.5, 0.5, 0.5, 0.0],
                [0.0, 0.5, 0.5, 1.5, 0.0, 1.5],
                [0.0, 0.5, 0.0, 0.0, 1.5, 0.5]
             ]



def calc_multiplier(l):
    neutral = 0
    effective = 0
    resists = 0
    for x in l:
        if x == 0.0: neutral += 1
        elif x == 0.5: resists += 1
        elif x == 1.5: effective += 1
    diff = effective - resists
    if diff > 0: return 1.0 + diff * 0.5
    elif diff < 0: return 1.0 * 0.5 ** abs(diff)
    else: return 1.0

def get_multiplier(a, d):
    x = []
    for at in a:
        for dt in d:
            if at and dt:
                m = multiplier[spice[dt]][spice[at]]
                x.append(m)
    return calc_multiplier(x)

def type_combinations(n):
    l = [('', x) for x in spice.keys()[:]]
    n -= 1
    while n > 0:
        for c in l[:]:
            for t in spice:
                if t not in c:
                    tmp = [x for x in c]
                    tmp.append(t)
                    l.append(tuple(tmp[:]))
        n -= 1
    return list(set(l))
    

def list_multipliers(n1=1, n2=1):
    l = []
    at = type_combinations(n1)
    dt = type_combinations(n2)
    
    for a in at:
        for d in dt:
            l.append((set(list(a)[1:]), set(list(d)[1:]), get_multiplier(a, d)))
    ln = []
    for c in l:
        if c not in ln:
            ln.append(c)
    return [(tuple(x[0]), tuple(x[1]), x[2]) for x in ln]




    
if __name__ == "__main__":
    
    """
    l = list_multipliers(n1=2, n2=2)
    ls = sorted(l, key = lambda x: x[-1])
    
    atk = {}
    de = {}
    for c in ls:
        atk[c[0]] = atk.get(c[0], 0) + c[2]
        de[c[1]] = de.get(c[1], 0) + c[2]
    for s in atk:
        pass
        print("%25s atk sum %6.2f" % (s, atk[s]))
    print()
    for s in de:
        pass
        print("%25s def sum %6.2f" % (s, de[s]))
    for c in ls:
        print("Atk: %24s Def: %24s Mult: %6.3f" % c)
    """
    
    print("%15s | ev: %6.2f" % ("Tussari", 25*1.5*(15.0/20.0)))
    print("%15s | ev: %6.2f" % ("Sniper", 25*1.0*(10.0/20.0)))
    print("%15s | ev: %6.2f" % ("CQC+", 25*1.5*(20.0/20.0)))
    print("%15s | ev: %6.2f" % ("Omaewa", 25*1.0*(15.0/20.0)))
    print("%15s | ev: %6.2f / %6.2f" % ("Antitankkiase", 25*0.5*(15.0/20.0), 25*2.0*(15.0/20.0)))
    print("%15s | ev: %6.2f" % ("Batturabensas", 10.5 * 3))

