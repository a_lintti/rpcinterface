
#include <cmath>

#include <vector>
#include <string>

#include "rayman.h"


std::vector<std::string> Gun::to_string() {
    std::vector<std::string> v;
    v.push_back(name + " (" + std::to_string(id) + ")");
    v.push_back("range: " + std::to_string(min_range) + "-" + std::to_string(max_range));
    v.push_back("multiplier: " + std::to_string(multiplier));
    v.push_back(description);
    return v;
}

float spice_advantage(Rayman def, Rayman att) {
    return 1.0;
}

int damage(float multiplier, int def, int atk, int roll) {
    return int(ceil(multiplier*float(atk)*25.0/float(def))) + roll;
}

int damage(Rayman def, Rayman att, Gun gun, int roll) {
    float multiplier = 1.0;
    multiplier *= spice_advantage(def, att);
    multiplier *= gun.multiplier;
    return damage(multiplier, def.def, att.atk, roll);
}



