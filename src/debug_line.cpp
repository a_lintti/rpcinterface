
#include "debug_line.h"

#include <vector>
#include <string>
#include <iostream>
#include <iterator>
#include <sstream>
#include <random>

#include <raylib.h>

#include "rayman.h"
#include "state.h"

std::map<int, char> keycode_to_char()
{
    std::map<int, char> m;
    // misc
    m[39] = '\'';
    m[44] = ',';
    m[45] = '-';
    m[46] = '.';
    m[47] = '/';
    m[32] = ' ';
    // numbers
    m[48] = '0';
    m[49] = '1';
    m[50] = '2';
    m[51] = '3';
    m[52] = '4';
    m[53] = '5';
    m[54] = '6';
    m[55] = '7';
    m[56] = '8';
    m[57] = '9';
    // letters
    m[97] = 'a';
    m[98] = 'b';
    m[99] = 'c';
    m[100] = 'd';
    m[101] = 'e';
    m[102] = 'f';
    m[103] = 'g';
    m[104] = 'h';
    m[105] = 'i';
    m[106] = 'j';
    m[107] = 'k';
    m[108] = 'l';
    m[109] = 'm';
    m[110] = 'n';
    m[111] = 'o';
    m[112] = 'p';
    m[113] = 'q';
    m[114] = 'r';
    m[115] = 's';
    m[116] = 't';
    m[117] = 'u';
    m[118] = 'v';
    m[119] = 'w';
    m[120] = 'x';
    m[121] = 'y';
    m[122] = 'z';
    
    m[65] = 'a';
    m[66] = 'b';
    m[67] = 'c';
    m[68] = 'd';
    m[69] = 'e';
    m[70] = 'f';
    m[71] = 'g';
    m[72] = 'h';
    m[73] = 'i';
    m[74] = 'j';
    m[75] = 'k';
    m[76] = 'l';
    m[77] = 'm';
    m[78] = 'n';
    m[79] = 'o';
    m[80] = 'p';
    m[81] = 'q';
    m[82] = 'r';
    m[83] = 's';
    m[84] = 't';
    m[85] = 'u';
    m[86] = 'v';
    m[87] = 'w';
    m[88] = 'x';
    m[89] = 'y';
    m[90] = 'z';
    return m;
}

DebugLine::DebugLine() {
    x = 30;
    y = 20;
    width = 40;
    height = 38;
    font_size = 20;
    line_height = 22;
    cursor_pos = 0;
    current_line = "";
    update_content();
    command_map.insert(std::pair<std::string, int>("cowsay", 1));
    command_map.insert(std::pair<std::string, int>("d", 2));
    command_map.insert(std::pair<std::string, int>("dam", 2));
    command_map.insert(std::pair<std::string, int>("damage", 2));
    command_map.insert(std::pair<std::string, int>("d20", 3));
    command_map.insert(std::pair<std::string, int>("d6", 4));
    command_map.insert(std::pair<std::string, int>("guns", 5));
    command_map.insert(std::pair<std::string, int>("g", 6));
    command_map.insert(std::pair<std::string, int>("gun", 6));

    
    keycode_to_char_map = keycode_to_char();
}

std::vector<std::string> DebugLine::list_commands() {
    std::vector<std::string> sv;
    sv.push_back("d/dam/damage [multiplier] [atk] [def] [roll]");
    sv.push_back("d20/d6");
    sv.push_back("guns");
    sv.push_back("g/gun [id]");
    return sv;
}


void DebugLine::update(State &state) {
    int key = GetKeyPressed();
    auto it = keycode_to_char_map.find(key);
    if (it != keycode_to_char_map.end()) {
        add_letter(keycode_to_char_map[key]);
    }
    if (IsKeyPressed(KEY_BACKSPACE)) {
        remove_letter();
    }
    if (IsKeyPressed(KEY_LEFT)) {
        move_cursor(false);
    }
    if (IsKeyPressed(KEY_RIGHT)) {
        move_cursor(true);
    }
    if (IsKeyPressed(KEY_ENTER)) {
        execute_line(state);
    }
}

void DebugLine::set_position(int _x, int _y) {
    x = _x;
    y = _y;
}

void DebugLine::update_content() {
    std::string l = ">> " + current_line;
    if (!content.empty()) content.pop_back();
    add_content(l);
}

void DebugLine::add_content(std::string c) {
    while (c.length() > 0) {
        int c_end = std::min(int(c.length()), width);
        std::string nc = c.substr(0, c_end);
        c = c.substr(c_end, c.length());
        content.push_back(nc);
    }
    while (content.size() > height) {
        content.erase(content.begin());
    }
}

void DebugLine::add_letter(char letter) {
    current_line.insert(cursor_pos, 1, letter);
    ++cursor_pos;
    update_content();
}

void DebugLine::remove_letter() {
    if (cursor_pos == 0) return;
    current_line.erase(cursor_pos-1);
    --cursor_pos;
    update_content();
}

void DebugLine::move_cursor(bool direction) {
    // true -> right
    if (direction) {
        if (cursor_pos < current_line.length()) {
            ++cursor_pos;
        }
    } else {
        if (cursor_pos > 0) {
            --cursor_pos;
        }
    }
}

void DebugLine::execute_line(State &state) {
    std::stringstream ss(current_line);
    std::istream_iterator<std::string> begin(ss);
    std::istream_iterator<std::string> end;
    std::vector<std::string> line_command(begin, end);
    
    if (!line_command.empty()) {
        int c = 0;
        auto it = command_map.find(line_command.front());
        if (it != command_map.end()) {
            c = command_map[line_command.front()];
        }
        switch (c) {
            case 0: {
                add_content("No command '" + line_command.front() + "' found.");
                break;
            }
            case 1: {
                std::cout << "MOOOO \n";
                add_content("MOOO");
                break;
            }
            case 2: {
                if (line_command.size() != 5) {
                    add_content("Damage (d) needs 4 parameters: multiplier, atk, def and roll");
                    break;
                }
                float multiplier = stof(line_command.at(1));
                int atk = stoi(line_command.at(2));
                int def = stoi(line_command.at(3));
                int roll = stoi(line_command.at(4));
                int dam = damage(multiplier, def, atk, roll);
                add_content("Damage: " + std::to_string(dam));
                break;
            }
            case 3: {
                int roll = 1+rand()%20;
                add_content("You rolled: " + std::to_string(roll));
                break;
            }
            case 4: {
                int roll = 1+rand()%6;
                add_content("You rolled: " + std::to_string(roll));
                break;
            }
            case 5: {
                std::map<int, Gun>::iterator it;
                for (it = state.guns.begin(); it != state.guns.end(); it++) {
                    add_content(it->second.name + ": " + std::to_string(it->first));
                }
                break;
            }
            case 6: {
                if (line_command.size() != 2) {
                    add_content("Gun (g) needs 1 parameter: id");
                    break;
                }
                int id = stoi(line_command.at(1));
                std::vector<std::string> sv = state.guns[id].to_string();
                for (std::string s : sv) {
                    add_content(s);
                }
                break;
            }
        }
        history.push_back(current_line);
    }
    current_line = "";
    cursor_pos = 0;
    add_content(">> ");
    update_content();
}

void DebugLine::draw(Font font) {
    
    // cursor
    float spacing = 1;
    int cursor_y = y+(content.size())*line_height;
    int cursor_x = x+(cursor_pos+3)*(float(MeasureTextEx(font, content.back().c_str(), font_size, spacing).x)/content.back().length());
    DrawRectangle(cursor_x, cursor_y, MeasureTextEx(font, "a", font_size, spacing).x+2, font_size, GREEN);

    // border
    DrawRectangleLinesEx(
        (Rectangle) {   
            x-10, 
            y+line_height-5,
            (width+5)*MeasureTextEx(font, "w", font_size, spacing).x, 
            height*line_height+10, 
        },
        3,
        WHITE
    );
    // Debug line content
    int l = 0;
    for (auto i = content.begin(); i != content.end(); ++i) {
        ++l;
        //if (l > height) break;
        DrawTextEx(font, i->c_str(), 
                   (Vector2) {x, y+l*line_height}, 
                   font_size, 
                   spacing, 
                   WHITE
        );
    }
}
