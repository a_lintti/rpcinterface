
#include <cmath>

#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <raylib.h>

#include "debug_line.h"
#include "rayman.h"
#include "state.h"


Font font;





void init() {
    const int screenWidth = 1200;
    const int screenHeight = 900;
    InitWindow(screenWidth, screenHeight, "RPCInterface");

    font = LoadFont("../font/Courier_Prime/Courier_Prime_Bold.ttf");

    SetTargetFPS(60);
}

void de_init() {
    UnloadFont(font);
    CloseWindow();
}


void render(DebugLine dl, State state) {
	BeginDrawing();
		ClearBackground((Color){ 40, 20, 20, 255 });
		
        // debug line
        dl.draw(font);
        
        // commands
        DrawTextEx(font, "Commands:", (Vector2) {580, 35}, 24, 0, WHITE);
        int l = 0;
        for (std::string s : dl.list_commands()) {
            DrawTextEx(font, s.c_str(), (Vector2){580, 64 + l*20}, 18, 0, WHITE);
            l++;
        }
        DrawRectangleLinesEx(
            (Rectangle) {   
                570, 
                58,
                600, 
                (l+1)*20-5 
            },
            3,
            WHITE
        );

        // spice table
        int s_table_x = 600;
        int s_table_y = 500;
        int text_y_offset = 11;
        int text_x_offset = 3;
        int font_size = 16;
        int rect_w = 70;
        int rect_h = 40;
        std::vector<Color> spice_color = {
            (Color){ 220, 180, 0, 255 }, 
            (Color){ 240, 80, 180, 255 }, 
            (Color){ 60, 60, 60, 255 }, 
            (Color){ 170, 170, 170, 255 },
            (Color){ 0, 200, 40, 255 },
            (Color){ 200, 40, 50, 255 },
            (Color){ 0, 120, 220, 255 } 
        };
        std::vector<Color> multiplier_color = {
            (Color){ 120, 0, 44, 255 },
            (Color){ 0, 52, 132, 255 },
            (Color){ 0, 117, 44, 255 }
        };

        DrawTextEx(
            font, 
            "Attacker", 
            (Vector2) {
                s_table_x+rect_w+2, 
                s_table_y-35
            }, 
            30, 
            15, 
            WHITE
        );
        std::vector<std::string> defender = {"D","e","f","e","n","d","e","r"};
        int def_l = 0;
        for (std::string c : defender) {
            DrawTextEx(
                font, 
                c.c_str(), 
                (Vector2) {
                    s_table_x-25, 
                    s_table_y+rect_h+2+def_l*34
                }, 
                30,
                0, 
                WHITE
            );
            def_l++;
        }
        for (int i = 0; i < 7; i++) {
            // horizontal name row
            Rectangle rh = (Rectangle) {   
                s_table_x + (i+1)*rect_w,
                s_table_y,
                rect_w, 
                rect_h 
            };
            DrawRectangleRec(rh, spice_color.at(i));
            DrawRectangleLinesEx(rh, 1, WHITE);
            DrawTextEx(
                font,
                state.spices[i].name.c_str(),
                (Vector2) { 
                    s_table_x + (i+1)*rect_w + text_x_offset, 
                    s_table_y + text_y_offset
                },
                font_size,
                0, 
                WHITE
            );
            // vertical name row
            Rectangle rv = (Rectangle) {   
                s_table_x,
                s_table_y + (i+1)*rect_h,
                rect_w, 
                rect_h 
            };
            DrawRectangleRec(rv, spice_color.at(i));
            DrawRectangleLinesEx(rv, 1, WHITE);
            DrawTextEx(
                font,
                state.spices[i].name.c_str(), 
                (Vector2) {
                    s_table_x + text_x_offset, 
                    s_table_y + (i+1)*rect_h + text_y_offset
                },
                font_size,
                0,
                WHITE
            );
            // multipliers
            for (int j = 0; j < 7; j++) {
                Rectangle rm = (Rectangle) {   
                    s_table_x + (j+1)*rect_w,
                    s_table_y + (i+1)*rect_h,
                    rect_w, 
                    rect_h 
                };
                float multiplier = state.spice_table[i][j];
                if (multiplier < 1.0) {
                    DrawRectangleRec(rm, multiplier_color.at(0));
                } else if (multiplier > 1.0) {
                    DrawRectangleRec(rm, multiplier_color.at(2));
                } else {
                    DrawRectangleRec(rm, multiplier_color.at(1));
                }
                DrawRectangleLinesEx(rm, 1, WHITE);
                std::stringstream stream;
                stream << std::fixed << std::setprecision(1) << multiplier;
                std::string s = stream.str();
                DrawTextEx(
                    font,
                    s.c_str(), 
                    (Vector2) {
                        s_table_x + (j+1)*rect_w + text_x_offset + 15, 
                        s_table_y + (i+1)*rect_h + text_y_offset - 2,
                    },
                    20,
                    0,
                    WHITE
                );
            }
        }


    EndDrawing();
}

int main(void) {
    // init raylib etc.
	init();

    DebugLine dl;
    State state;

	std::cout << "Starting loops\n";

    while (!WindowShouldClose()) {
        dl.update(state);
        render(dl, state);
    }

	
	// de-init after we're done
	de_init();

    return 0;
}
