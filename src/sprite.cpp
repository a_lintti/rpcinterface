

#include <raylib.h>

#include "sprite.h"


void Sprite::draw(float x, float y, float ang, float scale, Color c) {
    float ox, oy;
    ox = (frame % framesWide) * frameSize.x;
    oy = (int)(frame / framesWide) * frameSize.y;
    DrawTexturePro(texture, (Rectangle){ox, oy, frameSize.x,frameSize.y}, 
                            (Rectangle){x, y, frameSize.x * scale, frameSize.y * scale}, 
                            (Vector2){origin.x * scale, origin.y * scale}, ang, c);
}

void Sprite::next_frame() {
    frame = (frame+1)%maxFrame;
}
