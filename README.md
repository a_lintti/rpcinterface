
RPCInterface


## rpci

Used to track teams and to calculate damage values during the game.

**functionalities**

- Show team info
    - All teams layout (just raymans (turn used or not) and tp:s)
    - Team layout (raymans, tp, atk, def, lp, 
      actions, guns (names), abilities (names), 
      spice (as a color), range_adv)
    - Rayman layout (same as team, but also show gun and ability descriptions)
- Calculate damage
    - give 2 raymans, attacking weapon, roll and optional multiplier
    - special cases for healing buffing etc.
    - update teams (ask in case it was just simulation)
    - option for auto death
- Import teams
- Debug line
    - changing rayman properties
    - importing teams
    - gun info
    - game rules


## rpc_team_maker

Program for creating RPC teams.

**functionalities**

- export/import teamName.json
- add team name
- add raymans
    - and name them
    - choose type
    - add abilities (give name, scale and description)
    - add stats, lp, guns, actions, range
- automatic pojo calculation


